from kivy.app import App
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.properties import ObjectProperty

from gpiozero import DistanceSensor
from gpiozero import LED

from kivy.config import Config
Config.set('graphics', 'width', '200')
Config.set('graphics', 'height', '400')


class DistanceLine(Widget):

    def __init__(self, **kwa):
        super(DistanceLine, self).__init__(**kwa)
        self.sensor = DistanceSensor(trigger=18, echo=24, max_distance=2)
        self.led = LED(21)

    def move(self):
        distance = round(self.sensor.distance * 100, 0) * 2
        if distance <= 100.0:
            self.length = distance
            self.led.on()
        elif distance <= 399.0:
            self.length = distance
            self.led.off()
        else:
            self.length = 0
            self.led.off()

class DistanceDisplay(Widget):
    
    line = ObjectProperty(None)
    
    def update(self, dt):
        self.line.move()
   



class DistanceApp(App):
    def build(self):

        app = DistanceDisplay()
        Clock.schedule_interval(app.update, 1.0/20.0)
        return app


if __name__ == '__main__':
    DistanceApp().run()
